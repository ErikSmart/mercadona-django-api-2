from django.shortcuts import render
# importante para las vistas
from rest_framework.views import APIView
# Para poder hacer la respuesta
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from .models import Producto
# En serializers.py es donde se definene las tablas que se van a mostrar y donde se tranfoman en Json
from .serializers import ProductoSerial

# Create your views here.


class ProductoLista(APIView):
    def get(self, request):
        prod = Producto.objects.all()[:20]
        datos = ProductoSerial(prod, many=True).data
        return Response(datos)


class ProductoDetalle(APIView):
    def get(self, request, pk):
        detalle = get_object_or_404(Producto, pk=pk)
        datos = ProductoSerial(detalle).data
        return Response(datos)
