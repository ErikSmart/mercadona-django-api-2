from django.urls import path
from django.contrib import admin
# (se esta usando JTW descomentar si se quiere usar esta forma rest_framework.authentication.TokenAuthentication)
from rest_framework.authtoken import views
# La mejor manera de autentificar logeo JWT no olvidar editar settings
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
# Documentacion tipo swagger nativa de python
from rest_framework.documentation import include_docs_urls


from api.views import ProductoDetalle, ProductoLista, \
    CategoriaLista, CategoriaDetalle, CategoriaCrear, SubCategoriaLista, \
    Contar, \
    UsuarioCrear, \
    Login

urlpatterns = [
    path('v1/productos/', ProductoLista.as_view(), name='producto_lista'),
    path('v1/categorias/', CategoriaLista.as_view(), name='categoria_lista'),
    path('v1/categoria/', CategoriaCrear.as_view(), name='categoria_crear'),
    path('v1/productos/<int:pk>', ProductoDetalle.as_view(),
         name='producto_detalle'),
    path('v1/categoria/<int:pk>', CategoriaDetalle.as_view(),
         name='categoria_detalle'),
    path('v1/categoria/<int:pk>/subcategoria/',
         SubCategoriaLista.as_view(), name='s_list'),
    path('contar/', Contar.as_view(), name='c_list'),
    path('crear-usuario/', UsuarioCrear.as_view(), name='crear_usuario'),
    # Otra forma de logearse no recomendada
    path('login-bad/', Login.as_view(), name='log'),
    # Mejor forma de logerse
    path('login/', views.obtain_auth_token, name='login'),
    # Logearse por jwt en python
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('docs/', include_docs_urls(title='La documentacion', public=False)),
]
