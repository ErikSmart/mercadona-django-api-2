from rest_framework import permissions
from rest_framework.permissions import BasePermission, SAFE_METHODS
# Llama al usuario
from django.contrib.auth.models import User


class IsOwnerOrReadOnly(permissions.BasePermission):
    mensaje = "No es propetario"
    # el metodo has_object_permission es obligatorio
    my_safe_method = ['GET', 'POST']

    def has_permission(self, request, view):
        # solo funciona con integers es el que llama el id del usuario
        usuario = request.user.id
        if request.method in self.my_safe_method and usuario == 4:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        return obj.owner == "4"
