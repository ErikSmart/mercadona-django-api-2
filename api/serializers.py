from rest_framework import serializers
from .models import Producto, Categoria, SubCategoria
from django.contrib.auth.models import User
# Neceario para poder escribir en la tabla del token
from rest_framework.authtoken.models import Token
# Usuario Actual
from rest_framework.fields import CurrentUserDefault


class ProductoSerial(serializers.ModelSerializer):
    owner = serializers.HiddenField(default=serializers.CurrentUserDefault())
    # imprime el usuario en el json
    usuario = serializers.CharField(
        read_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = Producto
        # Campos a mostrar todos pero podria ser fields = ['url', 'username', 'email', 'is_staff']
        fields = '__all__'


class CategoriaSerial(serializers.ModelSerializer):
    owner = serializers.HiddenField(
        default=serializers.CurrentUserDefault())

    class Meta:
        model = Categoria
        # Campos a mostrar todos pero podria ser fields = ['url', 'username', 'email', 'is_staff']
        fields = '__all__'


class SubCategoriaSerial(serializers.ModelSerializer):
    owner = serializers.HiddenField(
        default=serializers.CurrentUserDefault())

    class Meta:
        model = SubCategoria
        # Campos a mostrar todos pero podria ser fields = ['url', 'username', 'email', 'is_staff']
        fields = '__all__'


class ContarSerial(serializers.ModelSerializer):
    owner = serializers.HiddenField(
        default=serializers.CurrentUserDefault())

    class Meta:
        model = SubCategoria
        fields = ['descripcion']


class UserSerial(serializers.ModelSerializer):
    """ superusuario = serializers.HiddenField(
        default=serializers.CurrentUserDefault()) """

    class Meta:
        model = User
        fields = ['username', 'email', 'password']
    # con extra_kwargs le dice que solo va a ser de escritura
        extra_kwargs = {'password': {'write_only': True}}
    # Creando al usuario

    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username'],
        )
    # ocultamos el valor de la contraseña al registrarla
        user.set_password(validated_data['password'])
        user.save()
        Token.objects.create(user=user)
        return user
