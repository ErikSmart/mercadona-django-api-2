# Esta importacion simplifica y he implementa POST (en vista.py esta la forma larga y menos completa)
from django.db.models import Prefetch
from rest_framework.response import Response
from rest_framework import generics
from .models import Producto, Categoria, SubCategoria
from rest_framework.views import APIView
# Necesario para poderse autentificar (login)
from django.contrib.auth import authenticate
# Necesario para mostrar el estado del servidor
from rest_framework import status

# importar el archivo permisos de super usuario
from .permisos import IsOwnerOrReadOnly
# Permisos de Django para poder validad middelware
from rest_framework.permissions import (IsAuthenticated, IsAdminUser)
# En serializers.py es donde se definene las tablas que se van a mostrar y donde se tranfoman en Json
from .serializers import ProductoSerial, CategoriaSerial, SubCategoriaSerial, ContarSerial, \
    UserSerial
# Create your views here.


# generics.ListCreateAPIView Lista el contenido


class ProductoLista(generics.ListCreateAPIView):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerial
    # Owner o superusuario esta orden es la que restringe el acceso
    permission_classes = ([IsAuthenticated, IsOwnerOrReadOnly])

# generics.RetrieveDestroyAPIView Lista solo un elememnto y nos da la opcion delete


class ProductoDetalle(generics.RetrieveDestroyAPIView):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerial


class CategoriaLista(generics.ListAPIView):
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerial

# Sobre escribe queryset join con categoria_id


class SubCategoriaLista(generics.ListCreateAPIView):
    def get_queryset(self):
        queryset = SubCategoria.objects.filter(categoria_id=self.kwargs["pk"])
        return queryset
    serializer_class = SubCategoriaSerial


class CategoriaDetalle(generics.RetrieveAPIView):
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerial


class CategoriaCrear(generics.CreateAPIView):
    queryset = Categoria.objects.all()
    serializer_class = CategoriaSerial
    # Owner o superusuario esta orden es la que restringe el acceso
    permission_classes = ([IsAuthenticated, IsOwnerOrReadOnly])


# consulta slq Raw


class Contar(generics.ListAPIView):
    queryset = Categoria.objects \
        .raw('select api_categoria.id, api_producto.descripcion as xx, api_categoria.descripcion as c from api_producto join api_categoria on api_producto.id=api_categoria.id;')
    # for cont in queryset:
    #   print(cont.xx, cont.c)
    # serializer_class = ContarSerial

    def list(self, request):
        queryset = self.get_queryset()
        serializer = ContarSerial(list(queryset), many=True)
        return Response(serializer.data)


class UsuarioCrear(generics.CreateAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = UserSerial


class Login(APIView):
    permission_classes = ()

    def post(self, request):
        usuario = request.data.get('username')
        contra = request.data.get('password')
        user = authenticate(username=usuario, password=contra)
        if user:
            return Response({"token": user.auth_token.key})
        else:
            return Response({"error": "No tienes las credenciales"}, status=status.HTTP_400_BAD_REQUEST)
